import React, { useState } from "react";
import Button from "./Button";

function App() {
  const [message, setMessage] = useState("");

  const handleButtonClick = () => {
    setMessage("Hello, world!");
  };

  const handleDeleteClick = () => {
    setMessage("");
  };

  const [count, setCount] = useState(0);

  const handleIncrement = () => {
    setCount(count + 1);
  };

  const handleDecrement = () => {
    setCount(count - 1);
  };

  return (
    <div>
      <Button label="Click me" onClick={handleButtonClick} />
      {message && (
        <div>
          <p>{message}</p>
          <Button label="Delete" onClick={handleDeleteClick} />
        </div>
      )}
      <p>Count: {count}</p>
      <button onClick={handleIncrement}>Increment</button>
      <button onClick={handleDecrement}>Decrement</button>
    </div>
    
  );
}



export default App;